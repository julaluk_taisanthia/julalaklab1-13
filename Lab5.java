package day2;

public class Lab5 {
	public static void main(String[] args) {
		
		String myGender = "male";
		switch (myGender) {
		case "male":
			System.out.println("You are male");break;
		case "female":
			System.out.println("You are female");break;
		}
		
		//Lab 5. Switch case
		
		   /*
				 ��� 1 ���ҧ������Ѵ�ô �·��
			�ô A ��Ҥ�ṹ = 80
			�ô B ��Ҥ�ṹ = 70 
			�ô C ��Ҥ�ṹ = 60
			�ô D ��Ҥ�ṹ = 50 
			�ô F ��Ҥ�ṹ = 40
			�ô E ��Ҥ�ṹ�繤������ 
				 */
		

        int Score = 32;
        String Grade;
        if(Score >= 80){
            Grade = "A";
        }
        else if(Score >= 75){
            Grade = "B+";
        }
        else if(Score >= 70){
            Grade = "B";
        }
        else if(Score >= 65){
            Grade = "C+";
        }
        else if(Score >= 60){
            Grade = "C";
        }
        else if(Score >= 55){
            Grade = "D+";
        }
        else if(Score >= 50){
            Grade = "D";
        }
        else if(Score >= 40){
            Grade = "F";
        }
        else{
            Grade = "E";
        }
        
        System.out.print(" Input total score : ");
        System.out.println(" Grade = " + Grade);
        
        //Lab 5. Switch case (Option)
        
        /*
		 ��� 1 ���ҧ�������÷ӧҹ�ͧ�Կ�� ����Ѻ �֡ 5 ����¨�������顴������鹷���ͧ���� ����������ʴ������ Elevator is going to ��鹷�衴 �蹡���� G ���ʴ���ͤ���  "Elevator is going to G floor." 
		 */
        //Elevator is going to ��鹷�衴 �蹡���� G ���ʴ���ͤ���  "Elevator is going to G floor
        
        int floor = 2;
        switch (floor) {
            case 'G' :
                System.out.println("Elevator is going to ground floor." + "��� G");
                break;
            case '1' :
                System.out.println("Elevator is going to first floor." + "��� 1");
                break;
            case '2' :
                System.out.println("Elevator is going to second floor." + "��� 2");
                break;
            case '3' :
                System.out.println("Elevator is going to third floor." + "��� 3");
                break;
            case '4' :
                System.out.println("Elevator is going to the fourth floor" + "��� 4");
                break;
            case '5' :
                System.out.println("Elevator is going to the fifth floor." + "��� 5");
                break;
            default:
                System.out.println("Elevator don't know where to go.");
        }
        
        /*
  		 ��� 2 ����¹����� ��䫵�ͧ����ͷ����� ���� switch...case statement
  ��˹���� �ҡ�к�䫵��� 29 ����ʴ������ Small
  ��˹���� �ҡ�к�䫵��� 42 ����ʴ������ Medium
  ��˹���� �ҡ�к�䫵��� 44 ����ʴ������ Large
  ��˹���� �ҡ�к�䫵��� 48 ����ʴ������ Extra Large
  ��˹���� ��Ҿ�鹰ҹ �� Unknown
  		 */
         
        int size = 4;
        switch (size) {
          case 29:
            System.out.println("Small");
            break;
          case 42:
            System.out.println("Medium");
            break;
          case 44:
            System.out.println("Large");
            break;
          case 48:
            System.out.println("Extra Large");
            break;
          default:
              System.out.println("Unknown");
        }
	}

}
